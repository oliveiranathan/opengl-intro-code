#include <iostream>

#define GLFW_INCLUDE_GLCOREARB
#define GL_GLEXT_PROTOTYPES
#include <GLFW/glfw3.h>

#include <memory>
#include <vector>

void error_callback(int, const char* description)
{
	std::cerr << "Error: " << description << '\n';
}

void gl_debug_message_callback(unsigned, unsigned,
							   unsigned, unsigned,
							   int, const char* message,
							   const void*)
{
	std::cerr << "OpenGL debug: " << message << '\n';
}

class GLFW_initializer final
{
public:
	GLFW_initializer()
	{
		if (!glfwInit()) throw std::exception();
		glfwSetErrorCallback(error_callback);
	}
	~GLFW_initializer() { glfwTerminate(); }
	GLFW_initializer(const GLFW_initializer&) = delete;
	GLFW_initializer(const GLFW_initializer&&) = delete;
	GLFW_initializer& operator=(const GLFW_initializer&) = delete;
	GLFW_initializer& operator=(const GLFW_initializer&&) = delete;
};

class GLFW_window final
{
public:
	GLFW_window(int width, int height, const std::string& title)
	{
		glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
		window = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);
		if (!window) throw std::exception();
		glfwMakeContextCurrent(window);
		glDebugMessageCallback(gl_debug_message_callback, nullptr);
	}
	~GLFW_window() { glfwDestroyWindow(window); }
	GLFW_window(const GLFW_window&) = delete;
	GLFW_window(const GLFW_window&&) = delete;
	GLFW_window& operator=(const GLFW_window&) = delete;
	GLFW_window& operator=(const GLFW_window&&) = delete;
	[[nodiscard]] bool should_close() const { return glfwWindowShouldClose(window); }
	void swap_buffers() const { glfwSwapBuffers(window); }
private:
	GLFWwindow* window;
};

class Shader final
{
	friend class Program;
public:
	Shader(GLenum type, const char* code)
	{
		id = glCreateShader(type);
		glShaderSource(id, 1, &code, nullptr);
		glCompileShader(id);
	}
	~Shader() { glDeleteShader(id); }
	Shader(const Shader&) = delete;
	Shader(const Shader&&) = delete;
	Shader& operator=(const Shader&) = delete;
	Shader& operator=(const Shader&&) = delete;

private:
	GLuint id = 0;
};

class Program final
{
public:
	explicit Program(const std::vector<std::shared_ptr<Shader>>& shaders)
	{
		id = glCreateProgram();
		std::for_each(shaders.begin(), shaders.end(), [this] (const std::shared_ptr<Shader>& shader)
		{ glAttachShader(this->id, shader->id); });
		glLinkProgram(id);
	}
	~Program() { glDeleteProgram(id); }
	Program(const Program&) = delete;
	Program(const Program&&) = delete;
	Program& operator=(const Program&) = delete;
	Program& operator=(const Program&&) = delete;
	void use() const { glUseProgram(id); }

private:
	GLuint id;
};

class Buffer final
{
	friend class Vao;

public:
	Buffer(int size, const void* data, GLuint flags)
	{
		glCreateBuffers(1, &id);
		glNamedBufferStorage(id, size, data, flags);
	}
	~Buffer() { glDeleteBuffers(1, &id); }
	Buffer(const Buffer&) = delete;
	Buffer(const Buffer&&) = delete;
	Buffer& operator=(const Buffer&) = delete;
	Buffer& operator=(const Buffer&&) = delete;

private:
	GLuint id = 0;
};

class Vao final
{
public:
	Vao() { glCreateVertexArrays(1, &id); }
	~Vao() { glDeleteVertexArrays(1, &id); }
	Vao(const Vao&) = delete;
	Vao(const Vao&&) = delete;
	Vao& operator=(const Vao&) = delete;
	Vao& operator=(const Vao&&) = delete;
	void vertex_array_vertex_buffer(GLuint binding_index, const Buffer& buffer, GLint offset, GLint stride) const
	{ glVertexArrayVertexBuffer(id, binding_index, buffer.id, offset, stride); }
	void vertex_array_attrib_format(GLuint attrib_index, GLint size, GLenum type, GLboolean normalized,
							   GLuint relative_offset) const
	{ glVertexArrayAttribFormat(id, attrib_index, size, type, normalized, relative_offset); }
	void vertex_array_attrib_binding(GLuint attrib_index, GLuint binding_index) const
	{ glVertexArrayAttribBinding(id, attrib_index, binding_index); }
	void enable_vertex_array_attrib(GLuint index) const
	{ glEnableVertexArrayAttrib(id, index); }
	void bind() const { glBindVertexArray(id); };

private:
	GLuint id = 0;
};

static const char* vertex_shader_source =
"#version 460 core\n"
"layout (location = 0) in vec2 position;\n"
"layout (location = 1) in vec3 color;\n"
"out VS_OUT\n"
"{\n"
"vec3 color;\n"
"} vs_out;\n"
"void main(void)\n"
"{\n"
"vs_out.color = color;\n"
"gl_Position = vec4(position, 0, 1);\n"
"}\n";

static const char* fragment_shader_source =
"#version 460 core\n"
"out vec4 color;\n"
"in VS_OUT\n"
"{\n"
"vec3 color;\n"
"} fs_in;\n"
"void main(void)\n"
"{\n"
"color = vec4(fs_in.color, 1);\n"
"}\n";

static const float color[]{ 0.5, 0.5, 0.5, 1 };

static const float triangle_position[]{
		0.0, 0.5,
		-0.5, -0.5,
		0.5, -0.5
};

static const float triangle_color[]{
		1, 0, 0,
		0, 1, 0,
		0, 0, 1
};

int main()
{
	GLFW_initializer glfw_initializer;
	GLFW_window window{640, 480, "Hello, World!"};
	std::vector<std::shared_ptr<Shader>> shaders{
		std::make_shared<Shader>(GL_VERTEX_SHADER, vertex_shader_source),
		std::make_shared<Shader>(GL_FRAGMENT_SHADER, fragment_shader_source)
	};
	Program program{shaders};
	Vao vao;
	Buffer position_buffer{sizeof(triangle_position), triangle_position, GL_NONE};
	vao.vertex_array_vertex_buffer(0, position_buffer, 0, sizeof(float) * 2);
	vao.vertex_array_attrib_format(0, 2, GL_FLOAT, false, 0);
	vao.vertex_array_attrib_binding(0, 0);
	vao.enable_vertex_array_attrib(0);

	Buffer color_buffer{sizeof(triangle_color), triangle_color, GL_NONE};
	vao.vertex_array_vertex_buffer(1, color_buffer, 0, sizeof(float) * 3);
	vao.vertex_array_attrib_format(1, 3, GL_FLOAT, false, 0);
	vao.vertex_array_attrib_binding(1, 1);
	vao.enable_vertex_array_attrib(1);

	while (!window.should_close())
	{
		glClearBufferfv(GL_COLOR, 0, color);
		program.use();
		vao.bind();
		glDrawArrays(GL_TRIANGLES, 0, 3);
		window.swap_buffers();
		glfwPollEvents();
	}
	return 0;
}
